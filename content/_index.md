[params.layout.home]
  hide_blog = true

[[params.layout.home.section]]
  type = "post"
  weight = 1


[[params.layout.home.section]]
  type = "algorithms"
  limit = 4
  weight = 2 # Optional, choose layout order for section
