+++
title = "The Kerzboard Project"
date = 2019-11-02T00:54:48+01:00
description = ""
draft = false
toc = false
categories = ["Kerzboard"]
tags = ["electronic", "keyboard"]
[[copyright]]
  owner = "~G.E.N~"
  date = "2019"
  license = "cc-by-nc-sa-4.0"
+++

The Kerzboard project will be a Build Log as I learn how to make a split
mechanical keyboard from scratch.

My goal is to make a keyboard with this layout
![](,,/../images/kerzboard-v0-3.jpg)

I will try to do as much as possible myself, this includes the following:

* Finding a definite Layout
* Making a prototype
* Writing the firmware for the keyboard
* Creating a "finished" split keyboard

I have no experience in making embedded projects such as this one, so this might
take some time, but I will keep you posted on my progress.
