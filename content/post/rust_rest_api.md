+++
title = "Rust_rest_api"
date = 2020-02-27T00:00:17+01:00
description = "Building a REST API with Rocket"
draft = true
toc = false
categories = ["programming"]
tags = ["rust", "programming", "api", "rocket", "diesel"]
[[copyright]]
  owner = "Arzach"
  date = "2020"
  license = "cc-by-nc-sa-4.0"
+++

TODO:
Fix panic when cannot connect to database

I wrote a small JSON REST CRUD API in Rust.
It models the backend of a dumbed down Wiki composed of documents, and their
revisions (versions).

``` bash
# These are the files we'll write to make this API a reality
$ tree src/
src
|-- error.rs            # our API error types
|-- lib.rs              # definition of our API
|-- main.rs             # To create a binary that uses our library
|-- models              # Types of objects used in our API
|   |-- mod.rs
|   |-- document.rs
|   `-- revision.rs
|-- repository          # Store code that interacts with the database
|   |-- mod.rs
|   |-- documents.rs
|   `-- revisions.rs
|-- routes              # handlers for HTTP requests
|   |-- mod.rs
|   `-- documents.rs
|-- schema.rs           # used by Diesel for it's ORM
`-- tests.rs            # unit tests
```

The wiki is just a set of `Document`s that we can read, create, edit & delete.
We keep track of the changes to a `Document` through `Revision`s. We can ask
for a specific version of the `Document` by adding a timestamp to our request.

Here is what calling our API will look like for our users:

```
$ curl http://wiki-api.com/documents
{"documents": [{"id":1,"title":"Dope topic"}]}

$ curl http://wiki-api.com/documents/Dope topic/latest
XXXX
$ curl http://wiki-api.com/documents/Dope topic/2020-03-28T09:30:03Z
XXXX
$ curl http://wiki-api.com/documents/This documents does not exist
XXXX
```

The main libraries that we will use to get there are `Diesel` to connect and
query a Database, and `Rocket` an elegant HTTP server to handle our users
requests.

``` bash
################################################################################
################### Configuration Files for Rust et al. ########################
################################################################################

$ cat Cargo.toml
[package]
name = "rust_json_api"
version = "0.1.0"
authors = ["arthamys"]
edition = "2018"

[dependencies]
rocket = "0.4.2"                                                # HTTP Server
serde = "1.0"
serde_derive = "1.0"
serde_json = "1.0"
diesel = { version = "1.4", features = ["postgres", "chrono"] } # Database ORM
diesel_migrations = "1.4"
chrono = { version = "0.4.10", features = ["serde"] }           # For timestamps

[dependencies.rocket_contrib]
version = "0.4.2"
features = ["json", "diesel_postgres_pool"]                     # DB connection
                                                                # and more
$ cat diesel.toml
[print_schema]
file = "src/schema.rs"

$ cat Rocket.toml
[global.databases]
pgdb = { url = "postgres://diesel@localhost:5432/wiki" }

################################################################################
###################### Setting up the database for our API #####################
################################################################################

$ docker run --name rust_json_api        \
             -p 5432:5432                 \
             -e POSTGRES_PASSWORD=password \
             -d postgres

$ echo <<EOF> create_db.sql
CREATE ROLE diesel WITH LOGIN;
CREATE DATABASE wiki WITH OWNER diesel;
EOF

$ PGPASSWORD=password psql -h localhost -U postgres < create_db.sql
```

Okay, now that we have covered these shenanigans, let's start writing code!

First, we'll look at how we start our HTTP server, and how we handle requests
on a route.

In `lib.rs`:
``` rust
#![feature(proc_macro_hygiene, decl_macro)] // `routes![/*...*/]` macro
#[macro_use] extern crate rocket;           // #[get(/*...*/)] macro

//   ________ Http method
//  /     ___ Route's slug
// /     /         __ return type has to implement `rocket::response::Responder`
#[get("/hello")] //   Some types, like String, implement it by default
fn greeting() -> String {
    "Hello, world!".to_string()
}

pub fn rocket() -> rocket::Rocket {
    rocket::ignite() // This doesn't run the server but creates it's "config"
	    .mount("/v0", // <- mount point of the api
	        routes![
	            greeting,
	        ]
	    )
}
```

And we will write a simple main that invoques our library code:

In `main.rs`:
``` rust
use rust_json_api as api;

fn main() {
  api::rocket().launch();
}
```

Time to make our machine sweat a little by building & running it

``` bash
$ cargo run
...
🔧 Configured for development.
    => address: localhost
    => port: 8000
    => log: normal
    => workers: 16
    => secret key: generated
    => limits: forms = 32KiB
    => keep-alive: 5s
    => tls: disabled
🛰  Mounting /v0:
    => GET /v0/hello (greeting)
🚀 Rocket has launched from http://localhost:8000
```

And now we can invoke that simple route, invoking it through curl:

``` bash
$ curl localhost:8000/v0/hello
Hello, world!
```

Okay, now we know the ultra basics of Rocket, let's see how to create a handler
that is more realistic, and update this handler to receive a JSON payload, and
answer with a JSON message.

And since we are dealing with external data, that means error handling, so
we'll add that too.

In `lib.rs`
``` rust
#[macro_use] extern crate serde_derive;      // to derive Deserialize
use rocket_contrib::json::{Json, JsonValue}; // to model the Json payload

// This allows the enum to be returned to Rocket so it auto serializes it
#[derive(Responder, Debug)]
pub enum Error {
    // Pretty explicit, this variant returns a 400 with a json payload
    #[response(status=400, content_type="json")]
    InvalidProperty(JsonValue)
}

// What we expect to receive from our clients
#[derive(Deserialize)]
struct ClientForm {
    pub name: String
}

//                   __ Requires the "Content-Type" header be "application/json"
//                  /                      __ binds the payload to this variable
//                 /                      /   and automatically deserialize it
//                /                      /
#[post("/hello", format="json", data="<client_form>")]
fn greeting(client_form: Json<ClientForm>) -> Result<JsonValue, Error> {
    if client_form.name.is_empty() { // Some basic error checking
        return Err(
            Error::InvalidProperty(
                json!({
                    "error": "Missing property content: 'name'".to_string()
                })));
    }
    Ok(json!({"message": format!("Hello, {}!", client_form.name)}))
}
```

Wow, now we have a way to receive JSON body parameters, and we answer with some
JSON, let's test it out:

``` bash
$ curl -X POST localhost:8000/v0/hello    \
       -H "Content-Type: application/json" \
	   -d '{"name": "Billy"}'
{"message":"Hello, Billy!"}

curl -vX POST localhost:8000/v0/hello      \
        -H "Content-Type: application/json" \
	    -d '{"name": ""}'
#...
< HTTP/1.1 400 Bad Request
< Content-Type: application/json
< Server: Rocket
< Content-Length: 44
#...
{"error":"Missing property content: 'name'"}
```

Okay, not too shaby, we even have a little bit of error handling with this.
Not that I would call that statisfactory, but it's a start.

That's pretty much all we need to know about Rocket to build our API.
To recap, with 25 lines of code, we have started an HTTP server that
automatically deserializes JSON parameters for our route, and allows us to write
a _type safe_ handler, that responds some JSON.

Now to make it something that's usefull, we need to do more than
responding hardcoded strings. So let's use [**Diesel**](https://diesel.rs/)
to persist data and serve it to our users.

I'm not going to explain how to setup Diesel, but I will point you to their
[getting started guide](https://diesel.rs/guides/getting-started/)
and [docs](https://docs.rs/diesel/latest/diesel/index.html)

``` bash
# Install diesel's CLI tool
# Change the features to match the database you use, or don't give any flags
# to support mysql, sqlite and postgres
$ cargo install diesel_cli --no-default-features --features "postgres"

# Create the config files for the tool
$ echo "DATABASE_URL=postgres://diesel:password@localhost/wiki" > .env
$ diesel setup
$ diesel migration generate inital_tables

# The path will change as it is versioned by creation time
$ cat <<EOF> /migrations/2020-02-25-184230_initial_tables/up.sql
CREATE TABLE documents (
  id SERIAL PRIMARY KEY,
  title VARCHAR(50) UNIQUE NOT NULL
);

CREATE TABLE revisions (
  id SERIAL PRIMARY KEY,
  content TEXT NOT NULL,
  creation_date TIMESTAMP NOT NULL,
  document_id INTEGER NOT NULL,
  FOREIGN KEY(document_id) REFERENCES documents(id)
);
EOF

$ cat <<EOF> /migrations/2020-02-25-184230_initial_tables/down.sql
DROP TABLE revisions;
DROP TABLE documents;
EOF

# apply the migration & redo, to test that our down migration also works
$ diesel migration run && diesel migration redo
```

Okay, sorry for the amount of shell in this post, I know you came for Rust.

I haven't forgotten, so let's dive in. First, an overview of where we are going

``` bash
# These are the files that deal with the database
$ tree src/
src
|-- ...
|-- models            # Represent the Rust mapping of our database types
|   |-- mod.rs        # You can think of them kind of like Data Access Objects
|   |-- document.rs   # if you're familiar with this design pattern.
|   `-- revision.rs
|-- repository        # I'm taking the Java nomeclature here; repositories
|   |-- mod.rs        # replace the SQL statements you would scribble by hand.
|   |-- documents.rs  # They are expressed using Diesel queries, that are
|   `-- revisions.rs  # checked for validity at compile-time.
|-- ...
|-- schema.rs         # Generated by diesel, nothing interesting there.
`-- ...               # It's used to write more comprehensible queries.
```

Okay, now for some files that actually bring something to the table
(pun intended).

``` rust
// models/document.rs

/// This is the type that we will use when retrieving Documents through Diesel
/// It's also the object we will send back to our clients
#[derive(Queryable, Serialize, Debug)]
pub struct Document {
    pub id: i32,
    pub title: String,
}

// repository/documents.rs

use diesel::{QueryResult, RunQueryDsl, QueryDsl, ExpressionMethods};
use diesel::pg::PgConnection;
use crate::models::document::Document;
use crate::schema::documents;

//Inserter Object
#[derive(Insertable)]
#[table_name="documents"]
struct NewDocument<'a> {
    title: &'a str,
}

/// persist a new document to the database
pub fn create(title: &str,
    conn: &PgConnection) -> QueryResult<Document> {
    let new_document = NewDocument {
        title: &title
    };

    // The equivalent SQL is
    // INSERT INTO documents (title) VALUES
    // ('Communication probes & where to hide them')

    diesel::insert_into(documents::table)
        .values(new_document)
        .get_result::<Document>(conn)
}
```

Okay so this is pretty straight forward code to write, but it makes use of a
PgConnection that we have not yet seen.
Managing connection pools to our database, and passing handles to each of our
handler can get messy very fast. Fortunately, Rocket provides a clean interface
to handle pooling.

``` rust
// repository/mod.rs

use diesel::pg::PgConnection;

// This identifier has to match what is configured in `Rocket.toml`
//          /
//         /____
#[database("pgdb")]
pub struct DbConn(PgConnection);

pub mod documents;

// lib.rs

use repository::DbConn;

pub fn rocket() -> rocket::Rocket {
    rocket::ignite()
      .mount("/v0",
          routes![
            // ...
          ])
      .attach(DbConn::fairing())
}
```

Now with this fairing attached to our Rocket, we are ready to launch !

Wait, no, we are missing a handler to call our new function that writes to the
database.

Let's slap together a little route for that:

``` rust
// lib.rs

#[macro_use] extern crate diesel;

use crate::repository::{documents, DbConn};
use rocket::response::status::Created;

#[post("/document/<title>")]
fn new_document(title: String,
                conn: DbConn) -> Result<Created<JsonValue>, JsonValue> {
    let document = match documents::create(&title, &conn) {
      Ok(_) => (), // everything went fine, the document has been persisted
      Err(e) => return Err(json!({"error": "Internal Server Error"}))
    };
   
    let resource_url = format!("{}/{}", "http://localhost:8000", &title);
    Ok(Created(resource_url, Some(json!({"document": document}))))
}
```

Okay, so with that ugly handler that we just hacked together, we can test that
we have correctly linked our handler with our database.

``` bash
$ curl -v -X POST localhost:8000/It is late and I want to sleep
...
< HTTP/1.1 201 Created
...
{"document":null}⏎
```
