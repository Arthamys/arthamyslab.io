+++
title = "There's my face on the stamp"
date = 2020-04-22T11:45:16+02:00
description = "Some notes on what to do when hosting your own mail server"
draft = false
toc = false
categories = ["notes"]
tags = ["mail", "notes", "postfix", "dovecot"]
[[copyright]]
  owner = "Arzach"
  date = "2020"
  license = "cc-by-nc-sa-4.0"
+++

I've set up a mail server recently, I'll leave some notes for when I'll have to
do it again.

## Crash course on email

``` txt
+-----------+                                     +-----------+
|Mail Client| 1                                   |Mail Client| 7
+---+-------+                                     +-----+-----+
    |                                                   |
   SMTP                                                IMAP
    |                         4                         |
    v 2            3         SMTP        5            6 v
 +--+--+  SMTP  +-----+    Bounces    +-----+  LMTP  +--+--+
 | MSA +------->+ MTA +-------------->+ MTA +------->+ MDA |
 +-----+        +-----+               +-----+        +-----+
```

Steps:

- 1 - Send an email from an **MUA** using **SMTP**
- 2 -  The **MSA** receives a new **submission**
- 3 -  Message gets sent to the outbound **MTA**
- 4 -  Message gets **bounced** to **MTA**s until it reaches its destination
- 5 -  Message gets delivered to an **MTA** on the destination **MX** server
- 6 -  Destination **MTA** passes the message to the **MDA** using **LMTP**
- 7 -  An **MUA** makes an **IMAP** request to fetch mail from the **MDA**

## What goes into setting up a mail server

### Postfix

Getting a mail server up and running is not that hard. We only need to
spin up a mail server, such as [Postfix](http://www.postfix.org/). It will be
both the **MTA** and **MSA** in the schema above.
It will take care of routing incoming SMTP messages to the right destination.

Most of the configuration that's needed to have a functioning mail server
has to do with postfix's files:

`ls /etc/postfix/{main,master}.cf`

Some usefull comands regarding postfix:

``` bash
# reload postfix without systemctl
postfix reload

# print the value of a postfix variable
postconf smtpd_tls_security_level

# set the value of a postfix variable
postconf -e 'smtpd_tls_security_level = may'

# print postfix version
postconf mail_version

# Search postfix map for a key in the given file
# see man postmap for more info on postfix maps
postmap -q "Received: from mail.[removed].com \
  (some_random_hostname [an.ip.v4.addr])" \
  pcre:/etc/postfix/header_checks.pcre
```

### Dovecot

To store the mail our server receives, and to fetch it from an email client,
we also need a **MDA**, such as [Dovecot](https://www.dovecot.org/).
Dovecot will be the destination for the messages meant for our mail domain.
It's job is to keep our mail organized, indexed, and other mailbox related
stuff.

It also handles the **IMAP**/**POP3** side of things.
Configuration for credentials, connection information that's needed for users
to access their mail, securing the connection to the mail server when fetching
mail also has to do with Dovecot (and Postfix).
There are a bunch of configuration files for it:

`ls /etc/dovecot/{dovecot.conf,conf.d/}`

## Receiving mail from other mail servers

To get mail servers in the world to find **our** mail server, we need to
register an MX DNS record that points to our domain.
The last field in the MX record is the priority for the MX server.
Usefull if we have more than one mail server.

``` txt
MX @ mail.domain.name 0
```

## Getting through spam filters

By default, mail from our server comes as spam to our recipients.
To get that fixed, we have a bunch of stuff to add, in order to prove that
mail coming from our server is legit, and not just spam mail.

* Support Reverse DNS lookup for the MX domain  
  Most spam filters check that the mail comes from a legit MX server by doing
  a DNS lookup of the sender's mail domain.

* A new DNS TXT record, for **SPF** such as `TXT @ v=spf1 mx ~all`  
  This prevents people from spoofing our domain name. The content of the
  record is mumbo jumbo to say that only our server is allowed to send
  mail from our domain.

* Add a support for **DKIM** on outgoing mail.  
  It's a way of authenticating our mail, by signing it with a private key
  before sending it out. Other mail servers then use our DKIM public key to
  verify the that the message was issued from our server.

* Share our **DKIM** public key with other mail server  
  we use another  DNS TXT record such as
  `TXT whatever._domainkey v=DKIM1;k=rsa;p=<public_key>`

Now with all that out of the way, our mail should no longer be considered spam
by other mail servers.

## Extra steps that should not be forgotten

* Enforcing TLS connections to our **MDA** so we don't end up leaking our
  password in plaintext if we use PLAIN SASL auth during IMAP requests
* Hardening **MTA** rules for incoming SMTP messages to avoid being an
  [open relay](https://www.wikiwand.com/en/Open_mail_relay)
* Forcing the **MTA** to only use TLSv1.2 and above

## Extras if you just can't get enough of configuring mail servers

* Advanced spam filtering (using 3rd party extensions such as spamassassin)
* Antivirus checks on incoming mail

Note that running a mail server with Postfix and Dovecot does not use a lot
of memory or CPU. Running beefy extensions like amavis (antivirus) or
spamassassin will take up a whole bunch of RAM. So go there at your own risks.

## Email lingo

* **MTA**: Mail Transfer Agent, that's a server that speaks **SMTP**
  It's job is to relay or transfer or even bounce emails to their correct
  destination.
* **MSA**: Mail Submission Agent, a server that handles mail requests.
  Its job is to hand off the mail to it's buddy the **MTA** for delivery.
* **MDA**: Mail Delivery Agent, the server that holds our mail until we request
  it through a mail client (a.k.a **MUA**)
* **MUA**: Mail User Agent, that's just a mail client, it can be a web
  interface, a CLI, an app on your phone, basically anything **you** as humans
  interract with to send and receive mail.
* **MX**: Mail Exchange, that's either a server, or a DNS entry that maps
  to a server. A **Mail Exchange** is basically an encapsulation of an **MSA**
  and an **MTA**
* **SMTP**: Simple Mail Transfer Protocol, defines the mail's
  **transport** (think of it as how a packet is routed). So SMTP only gives
  information on the **envelope** of an email. That is, sender and recipient,
  some headers but **not** the content of the email.
* **ESMTP**: Extended Simple Mail Transfer Protocol, as always
  with old protocols, they get extended to keep up with the new needs of the
  users. ESMTP adds UTF8, Authentication, message chunking and some other
  tidbits that keep SMTP relevant today.
* **LMTP**: Local Mail Transfer Protocol, same thing as **ESMTP** but
  meant for delivering mail to **MDU**s that don't have mail queues.
* **IMAP**: Internet Message Address Protocol, is used to interact
  with the **MDA**. So this protocol describes the actions that a **MUA** can 
  take on a mailbox, such as fetching mail (fresh and stale), deleting emails
  from an inbox, moving mail around, and such housekeeping tasks.
  It's **not** used to **send** email. You use **SMTP** to send emails, not
  IMAP.
* **POP3**: Post Office Protocol, fills the same shoes as IMAP, but it
  was designed for a usage where one would only briefly connect to the **MDA**
  to retrieve mail when the network was available, and store the messages
  client side. So I guess this is more often used on mobile devices, but
  don't quote me on that.
* **Bounce**: During delivery, mail can get to a **MTA** that is not the final
  destination. In that situation, the message can get **bounced** to another
  **MTA**.
* **SPF**: Sender Policy Framework, that's used to mitigate domain spoofing.
  It uses a DNS TXT record to notify other servers of the addresses of mail
  servers allowed to issued mail as our domain.
* **DKIM**: Domain Keys Identified Mail, this is a mail authentication
  mechanism that uses a public/private key signing to detect forged sender
  addresses in emails.
* **DMARC**: Domain-based Message Authentication, Reporting and Conformance,
  this one is a mouthful. This is another mitigation for spammers and email
  spoofing. It build atop **SPF** and **DKIM** and it's used
  to indicate to other mail servers what to do with offending mail (?) I'm not
  100% sure what it does actually.

## Resources

* `man postfix`
* https://www.linuxbabe.com/redhat/run-your-own-email-server-centos-postfix-smtp-server
* https://serverfault.com/a/998993
* https://testssl.sh/
* https://linux-audit.com/postfix-hardening-guide-for-security-and-privacy/
* https://upcloud.com/community/tutorials/secure-postfix-using-lets-encrypt/
* https://wiki2.dovecot.org/
* https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol
* https://www.wikiwand.com/en/Internet_Message_Access_Protocol
* https://en.wikipedia.org/wiki/Extended_SMTP
* https://en.wikipedia.org/wiki/Simple_Authentication_and_Security_Layer
* https://www.eff.org/

